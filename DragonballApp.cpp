#include "DragonballApp.h"
#include "GLUtils.hpp"

#include <math.h>

DragonballApp::DragonballApp(void)
{
	shaderProgramID = 0;
}

DragonballApp::~DragonballApp(void)
{
}

void DragonballApp::setGLSettings()
{
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glCullFace(GL_BACK); 
}

void DragonballApp::initShaders()
{
	GLuint vs_ID = loadShader(GL_VERTEX_SHADER,		"myVert.vert");
	GLuint fs_ID = loadShader(GL_FRAGMENT_SHADER,	"myFrag.frag");

	shaderProgramID = glCreateProgram();

	glAttachShader(shaderProgramID, vs_ID);
	glAttachShader(shaderProgramID, fs_ID);

	glBindAttribLocation( shaderProgramID, 0, "vs_in_pos");	
	glBindAttribLocation( shaderProgramID, 1, "vs_in_col");
	glBindAttribLocation( shaderProgramID, 2, "vs_in_tex0");

	glLinkProgram(shaderProgramID);

	glDeleteShader( vs_ID );
	glDeleteShader( fs_ID );
}

bool DragonballApp::isLinkingCorrect()
{
	GLint infoLogLength = 0, result = 0;

	glGetProgramiv(shaderProgramID, GL_LINK_STATUS, &result);
	glGetProgramiv(shaderProgramID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if ( GL_FALSE == result )
	{
		std::vector<char> ProgramErrorMessage( infoLogLength );
		glGetProgramInfoLog(shaderProgramID, infoLogLength, NULL, &ProgramErrorMessage[0]);
		fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);
        return false;
	}
    else
        return true;
}

void DragonballApp::unbindBuffers()
{
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void DragonballApp::setPerspective()
{
	m_matProj = glm::perspective( 45.0f, 640/480.0f, 1.0f, 1000.0f );
}

void DragonballApp::connectUniformVariables()
{
	m_loc_mvp = glGetUniformLocation( shaderProgramID, "MVP");
	m_loc_texture = glGetUniformLocation( shaderProgramID, "texture" );
}

void DragonballApp::loadTextures()
{
    lilDeathwingTexture = TextureFromFile("textures/whelp.bmp");
    grassTexture = TextureFromFile("textures/grass.jpg");
    fireTexture = TextureFromFile("textures/firetexture.bmp");
}

void DragonballApp::loadMeshes()
{
    lilDeathwing = ObjParser::parse("models/whelp.obj");
    lilDeathwing->initBuffers();
    std::cout << "Loaded whelp..." << std::endl;

    sphere = ObjParser::parse("models/sphere.obj");
    sphere->initBuffers();
    std::cout << "Loaded shpere..." << std::endl;

    loadGroundResources();
    std::cout << "Loaded ground into buffers..." << std::endl;
}

void DragonballApp::loadGroundResources()
{
	Vertex vert[] =
	{ 
		{glm::vec3(-5, 0, -7), glm::vec3( 0, 1, 0), glm::vec2(0, 0)},
		{glm::vec3(-5, 0, 13), glm::vec3( 0, 1, 0), glm::vec2(0, 1)},
		{glm::vec3( 5, 0, -7), glm::vec3( 0, 1, 0), glm::vec2(1, 0)},
		{glm::vec3( 5, 0, 13), glm::vec3( 0, 1, 0), glm::vec2(1, 1)},
	};

    GLuint indices[]=
    {
        0,1,2,
        2,1,3,
    };

    Vertex left_wall_vertices[] =
    {
		{glm::vec3(  5, 0, -7), glm::vec3( 0, 1, 0), glm::vec2(0, 0)},// 0
		{glm::vec3(4.5, 0, -7), glm::vec3( 0, 1, 0), glm::vec2(0, 1)},// 1
		{glm::vec3(  5, 3, -7), glm::vec3( 0, 1, 0), glm::vec2(0, 1)},// 2
		{glm::vec3(4.5, 3, -7), glm::vec3( 0, 1, 0), glm::vec2(0.5, 0.5)},// 3
		{glm::vec3(4.5, 0, 13), glm::vec3( 0, 1, 0), glm::vec2(1, 0)},// 4
		{glm::vec3(  5, 0, 13), glm::vec3( 0, 1, 0), glm::vec2(1, 0)},// 5
		{glm::vec3(  5, 3, 13), glm::vec3( 0, 1, 0), glm::vec2(1, 1)},// 6
		{glm::vec3(4.5, 3, 13), glm::vec3( 0, 1, 0), glm::vec2(0, 0)},// 7
    };

    GLuint left_wall_indices[] =
    {
        0,1,2,
        2,1,3,
        3,1,4,
        3,4,7,
        7,4,5,
        5,4,6,
        6,4,7,
        7,6,3,
        6,2,3,
        0,2,5,
        5,2,6,
    };
    
    wall_left = new Mesh();
    wall_left->setVertices(left_wall_vertices, sizeof(left_wall_vertices));
    wall_left->setIndices(left_wall_indices, sizeof(left_wall_indices));
    wall_left->initBuffers();

    ground = new Mesh();
    ground->setVertices(vert, sizeof(vert));
    ground->setIndices(indices, sizeof(indices));
    ground->initBuffers();
}

void DragonballApp::setGlobeWaypoints()
{
    waypoints_globe1.push_back(glm::vec3(-3.5, 1,-4));
    waypoints_globe1.push_back(glm::vec3( 3.5, 1,-4));
    waypoints_globe2.push_back(glm::vec3(-3.5, 1, 3));
    waypoints_globe2.push_back(glm::vec3( 3.5, 1, 3));
    waypoints_globe3.push_back(glm::vec3(-3.5, 1, 9));
    waypoints_globe3.push_back(glm::vec3( 3.5, 1, 9));
}

bool DragonballApp::Init()
{
	glClearColor(0.125f, 0.25f, 0.5f, 1.0f);

    setGLSettings();
    initShaders();

    if (!isLinkingCorrect())
        return false;

    setPerspective();
    connectUniformVariables();
    loadTextures();
    loadMeshes();

    dragon_position = glm::vec3(0,1,-7);
    dragon_rotation = glm::vec3(0,0,0);
    dragon_scale = glm::vec3(1, 1, 1);
    fireball_position = dragon_position;
    
    setGlobeWaypoints();

    param = 0.0f;
    param2 = 0.0f;
    param3 = 0.4f;
    currentPoint = 0;
    currentPoint2 = 1;
    currentPoint3 = 0;
    sphere1_position = glm::vec3(-3.5,0,4);
    sphere2_position = glm::vec3(-3.5,0,9);
    sphere3_position = glm::vec3(-3.5,0,13);
    start_time = 0;
    shoot = false;
    sphereSpeed = 0.006;
    sphereSpeed2 = 0.006;
    sphereSpeed3 = 0.006;
    sphere_lock = false;
    sphere2_lock = false;
    sphere3_lock = false;
    sphere_speed_change_start_time = SDL_GetTicks();
    srand(time(NULL));

	return true;
}

void DragonballApp::setBackgroundRedIfTouchedBall()
{
    if ((getDistance(sphere1_position, dragon_position) < 0.5) || //1.5-el sokkal realisabban nez ki...
        (getDistance(sphere2_position, dragon_position) < 0.5) ||
        (getDistance(sphere3_position, dragon_position) < 0.5))
    {
        start_time = SDL_GetTicks();
	    glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
    }

    int time = SDL_GetTicks();
    if (time > (start_time+3000))
	    glClearColor(0.125f, 0.25f, 0.5f, 1.0f);
}

void DragonballApp::checkSphereAndFireballCollision()
{
    if (!sphere_lock && getDistance(sphere1_position, fireball_position) < 1.5)  //1.5-el sokkal realisabban nez ki...
    {
        sphereSpeed *= -1;
        sphere_lock = true;
    }
    if (!sphere2_lock && getDistance(sphere2_position, fireball_position) < 1.5)
    {
        sphereSpeed2 *= -1;
        sphere2_lock = true;
    }
    if (!sphere3_lock && getDistance(sphere3_position, fireball_position) < 1.5)
    {
        sphereSpeed3 *= -1;
        sphere3_lock = true;
    }
}

void DragonballApp::calculateNextSpherePosition
(unsigned int& currentPointt, std::vector<glm::vec3>& waypoints, float& positionRatio, glm::vec3& spherePosition, float& sphereSpeed)
{
    unsigned int lastIndex = currentPointt == 0 ? waypoints.size() - 1: currentPointt - 1;
    spherePosition = (1-positionRatio) * waypoints[lastIndex] + positionRatio * waypoints[currentPointt];
    positionRatio += sphereSpeed;
    if (positionRatio >= 1.0f)
    {
        positionRatio = 0.0f;
        currentPointt = (currentPointt + 1) % waypoints.size();
    } 
}

void DragonballApp::Update()
{
    m_matView = glm::lookAt(glm::vec3(0, 10, -13),		
							dragon_position,
							glm::vec3(0,  1,  0));

    int temp_sdl = SDL_GetTicks();
    if (temp_sdl > sphere_speed_change_start_time+3000)
    {
        sphere_speed_change_start_time = SDL_GetTicks();
        sphereSpeed = (float)(rand() % 10 + 1) / 1000;
        sphereSpeed2 = (float)(rand() % 10 + 1) / 1000;
        sphereSpeed3 = (float)(rand() % 10 + 1) / 1000;
    }

    calculateNextSpherePosition(currentPoint, waypoints_globe1, param, sphere1_position, sphereSpeed);
    calculateNextSpherePosition(currentPoint2, waypoints_globe2, param2, sphere2_position, sphereSpeed2);
    calculateNextSpherePosition(currentPoint3, waypoints_globe3, param3, sphere3_position, sphereSpeed3);
    
    if (param < 0.0f)
        sphereSpeed *= -1;
    if (param2 < 0.0f)
        sphereSpeed2 *= -1;
    if (param3 < 0.0f)
        sphereSpeed3 *= -1;

    setBackgroundRedIfTouchedBall();

    fireball_position.z += cos(fireball_rotation_degree) / 2;
    fireball_position.x += sin(fireball_rotation_degree) / 2;

    checkSphereAndFireballCollision();
    shoot = checkFireballBoundaries();
}

void DragonballApp::drawMesh(Mesh* mesh, GLuint texture, glm::mat4 transformation)
{
	glUseProgram( shaderProgramID );

	m_matWorld = glm::translate<float>(0, 1, 0);
	glm::mat4 mvp = m_matProj * m_matView * transformation;
	glUniformMatrix4fv( m_loc_mvp, 1, GL_FALSE, &(mvp[0][0]) );

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUniform1i(m_loc_texture, 0);

	mesh->draw();

	glUseProgram( 0 );
}

void DragonballApp::drawGround()
{
    glm::mat4 notransform = glm::scale<float>(glm::vec3(1, 1, 1));
    glm::mat4 transformToRight = glm::translate<float>(glm::vec3(-9.5, 0, 0));
    drawMesh(ground, grassTexture, notransform);
    drawMesh(wall_left, grassTexture, notransform);
    drawMesh(wall_left, grassTexture, transformToRight);
}

void DragonballApp::drawSphere(glm::vec3 sphere_position)
{
    glm::mat4 transform = glm::scale<float>(1,1,1) * 
                          glm::translate<float>(sphere_position);
    drawMesh(sphere, fireTexture, transform);
}

void DragonballApp::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	drawGround();

    glm::mat4 transform = glm::scale<float>(dragon_scale) * 
                          glm::translate<float>(dragon_position) *
                          glm::rotate<float>(dragon_rotation.x, 1, 0, 0) *
                          glm::rotate<float>(dragon_rotation.y, 0, 1, 0) *
                          glm::rotate<float>(dragon_rotation.z, 0, 0, 1);
    drawMesh(lilDeathwing, lilDeathwingTexture, transform);
    drawSphere(sphere1_position);
    drawSphere(sphere2_position);
    drawSphere(sphere3_position);
    if (shoot)
    {
        drawSphere(fireball_position);
    }
}

bool DragonballApp::checkBoundaries()
{
    if ((dragon_position.z > 13)  || (dragon_position.z < -7) ||
        (dragon_position.x > 4.0) || (dragon_position.x < -4.0))
        return false;
    return true;
}

void DragonballApp::unlockLocks()
{
    sphere_lock = sphere2_lock = sphere3_lock = false;
}

bool DragonballApp::checkFireballBoundaries()
{
    if (!shoot)
        return false;
    if ((fireball_position.z > 13) || (fireball_position.z < -7) ||
        (fireball_position.x > 4.0) || (fireball_position.x < -4.0))
    {
        unlockLocks();
        return false;
    }
    return true;
}

int DragonballApp::getDistance(glm::vec3 position1, glm::vec3 position2)
{
    return sqrt(pow(position2.x-position1.x, 2)+pow(position2.y-position1.y, 2)+pow(position2.z-position1.z, 2));
}

void DragonballApp::KeyboardForward()
{
    glm::vec3 last_position = dragon_position;
    rotation_degree = dragon_rotation.y * 3.14 / 180;
    dragon_position.z += cos(rotation_degree) / 2;
    dragon_position.x += sin(rotation_degree) / 2;
    if (!checkBoundaries())
        dragon_position = last_position;
}

void DragonballApp::KeyboardBackward()
{
    glm::vec3 last_position = dragon_position;
    rotation_degree = dragon_rotation.y * 3.14 / 180;
    dragon_position.z -= cos(rotation_degree) / 2;
    dragon_position.x -= sin(rotation_degree) / 2;
     if (!checkBoundaries())
        dragon_position = last_position;
}

void DragonballApp::KeyboardLeft()
{
    dragon_rotation.y += 5;
    rotation_degree = dragon_rotation.y * 3.14 / 180;
}

void DragonballApp::KeyboardRight()
{
    dragon_rotation.y -= 5;
    rotation_degree = dragon_rotation.y * 3.14 / 180;
}

void DragonballApp::KeyboardShoot()
{
    if (!shoot)
    {
        shoot = true;
        fireball_position = dragon_position;
        fireball_rotation = dragon_rotation;
        fireball_rotation_degree = rotation_degree;
    }
}

void DragonballApp::MouseMove(SDL_MouseMotionEvent& mouse){}
void DragonballApp::MouseDown(SDL_MouseButtonEvent& mouse){}
void DragonballApp::MouseUp(SDL_MouseButtonEvent& mouse){}
void DragonballApp::MouseWheel(SDL_MouseWheelEvent& wheel){}

void DragonballApp::Resize(int width, int height)
{
	glViewport(0, 0, width, height);
	m_matProj = glm::perspective(  45.0f,		// 90 fokos nyilasszog
									width/(float)height,	// ablakmereteknek megfelelo nezeti arany
									0.01f,			// kozeli vagosik
									100.0f);		// tavoli vagosik
}

void DragonballApp::Clean()
{
	delete lilDeathwing;
    delete ground;
    delete wall_left;
    delete sphere;

    glDeleteTextures(1, &lilDeathwingTexture);
    glDeleteTextures(1, &grassTexture);
    glDeleteTextures(1, &fireTexture);

	glDeleteProgram( shaderProgramID );
}
