#pragma once

#include <GL/glew.h>

#include <SDL.h>
#include <SDL_opengl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#include "ObjectParser.h"
#include <vector>

#include "Vertex.h"

class DragonballApp
{
public:
	DragonballApp(void);
	~DragonballApp(void);

	bool Init();
	void Clean();

	void Update();
	void Render();

    void KeyboardForward();
    void KeyboardBackward();
    void KeyboardLeft();
    void KeyboardRight();
    void KeyboardShoot();
	void MouseMove(SDL_MouseMotionEvent&);
	void MouseDown(SDL_MouseButtonEvent&);
	void MouseUp(SDL_MouseButtonEvent&);
	void MouseWheel(SDL_MouseWheelEvent&);
	void Resize(int, int);
protected:
	glm::mat4 m_matWorld;
	glm::mat4 m_matView;
	glm::mat4 m_matProj;

	GLuint	m_loc_mvp;
	GLuint	m_loc_texture;
private:
    GLuint shaderProgramID;
    
    GLuint lilDeathwingTexture;
    GLuint grassTexture;
    GLuint fireTexture;
    GLuint redTexture;

    Mesh* lilDeathwing;
    Mesh* ground;
    Mesh* wall_left;
    Mesh* sphere;

    glm::vec3 dragon_position;
    glm::vec3 dragon_scale;
    glm::vec3 dragon_rotation;
    glm::vec3 sphere1_position;
    glm::vec3 sphere2_position;
    glm::vec3 sphere3_position;
    glm::vec3 fireball_position;
    glm::vec3 fireball_rotation;

    float rotation_degree;
    float fireball_rotation_degree;
   
    float param;
    float param2;
    float param3;
    float sphereSpeed;
    float sphereSpeed2;
    float sphereSpeed3;
    bool sphere_lock;
    bool sphere2_lock;
    bool sphere3_lock;
    
    unsigned int currentPoint;
    unsigned int currentPoint2;
    unsigned int currentPoint3; 
    std::vector<glm::vec3> waypoints_globe1;
    std::vector<glm::vec3> waypoints_globe2;
    std::vector<glm::vec3> waypoints_globe3;

    int start_time;
    bool shoot;
    int sphere_speed_change_start_time;

    void setGLSettings();
    void initShaders();
    bool isLinkingCorrect();
    void unbindBuffers();
    void setPerspective();
    void connectUniformVariables();
    void loadTextures();
    void loadMeshes();
    void loadGroundResources();
	
    void drawGround();
	void drawMesh(Mesh* mesh, GLuint texture, glm::mat4 transformation);
    void drawSphere(glm::vec3 sphere_position);

    void setGlobeWaypoints();
    void updateSpherePosition(glm::vec3* sphere_position, std::vector<glm::vec3>* waypoint);
    bool checkBoundaries();
    bool checkFireballBoundaries();
    void unlockLocks();
    void setBackgroundRedIfTouchedBall();
    void KeyboardUp(SDL_KeyboardEvent&);
    void checkSphereAndFireballCollision();
    void calculateNextSpherePosition(unsigned int& currentPointt, std::vector<glm::vec3>& waypoints, float& positionRatio, glm::vec3& spherePosition, float& sphereSpeed);


    int  getDistance(glm::vec3 position1, glm::vec3 position2);
};
